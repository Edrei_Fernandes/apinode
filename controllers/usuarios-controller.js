const mysql = require("../mysql").pool;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

exports.cadastrar = (req, res, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    connection.query(
      "SELECT * FROM usuarios WHERE email = ?",
      [req.body.email],
      (error, results) => {
        if (error) {
          return res.status(500).send({ error: error });
        }

        if (results.length > 0) {
          return res.status(409).send({ mensagem: "Usuário já cadastrado" });
        }

        bcrypt.hash(req.body.senha, 10, (errBcrypt, hash) => {
          if (errBcrypt) {
            return res.status(500).send({ error: errBcrypt });
          }

          connection.query(
            "INSERT INTO usuarios (email, senha) VALUES (?,?)",
            [req.body.email, hash],
            (error, results) => {
              connection.release();

              if (error) {
                return res.status(500).send({ error: error });
              }

              const resultado = {
                mensagem: "Usuário criado com sucesso",
                usuarioCriado: {
                  id_usuario: results.insertId,
                  email: req.body.email,
                },
              };

              return res.status(201).send(resultado);
            }
          );
        });
      }
    );
  });
};

exports.login = (req, res, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return res.status(500).send({ error: error });
    }

    connection.query(
      "SELECT * FROM usuarios WHERE email = ?",
      [req.body.email],
      (error, results, field) => {
        connection.release();

        if (error) {
          return res.status(500).send({ error: error });
        }

        if (results.length < 1) {
          return res.status(401).send({ mensagem: "Falha na autenticação" });
        }

        bcrypt.compare(req.body.senha, results[0].senha, (error, result) => {
          if (error) {
            return res.status(401).send({ mensagem: "Falha na autenticação" });
          }

          if (result) {
            const token = jwt.sign(
              {
                id_usuario: results[0].id_usuario,
                email: results[0].email,
              },
              process.env.JWT_KEY,
              {
                expiresIn: "1h",
              }
            );
            return res.status(200).send({
              mensagem: "Autenticado com sucesso",
              token: token,
            });
          }

          return res.status(401).send({ mensagem: "Falha na autenticação" });
        });
      }
    );
  });
};
