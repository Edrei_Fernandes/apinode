const mysql = require("../mysql");

exports.getProdutos = async (req, res, next) => {
  try {
    const query = "SELECT * FROM produtos";
    const result = await mysql.execute(query);

    const response = {
      quantidade: result.length,
      produtos: result.map((produto) => {
        return {
          id_produto: produto.id_produto,
          nome: produto.nome,
          preco: produto.preco,
          imagem_produto: produto.imagem_produto,
          request: {
            tipo: "GET",
            descricao: "Retorna detalhes de um produto",
            url: process.env.URL_API + "produtos/" + produto.id_produto,
          },
        };
      }),
    };

    return res.status(200).send(response);
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.postProduto = async (req, res, next) => {
  try {
    const query =
      "INSERT INTO produtos (nome, preco, imagem_produto) VALUE (?,?,?)";
    const result = await mysql.execute(query, [
      req.body.nome,
      req.body.preco,
      req.file.path,
    ]);

    const response = {
      mensagem: "Produto inserido com sucesso",
      produtoCriado: {
        id_produto: result.insertId,
        nome: req.body.nome,
        preco: req.body.preco,
        imagem_produto: req.file.path,
        request: {
          tipo: "GET",
          descricao: "Retorna todos os produtos",
          url: process.env.URL_API + "produtos",
        },
      },
    };

    return res.status(201).send(response);
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.getUmProduto = async (req, res, next) => {
  try {
    const query = "SELECT * FROM produtos WHERE id_produto = ?";
    const result = await mysql.execute(query, [req.params.id_produto]);

    if (result.length == 0) {
      return res.status(404).send({
        mensagem: "Não foi encontrado produto com este ID",
      });
    }

    const response = {
      produto: {
        id_produto: result[0].id_produto,
        nome: result[0].nome,
        preco: result[0].preco,
        imagem_produto: result[0].imagem_produto,
        request: {
          tipo: "GET",
          descricao: "Retorna todos os produtos",
          url: process.env.URL_API + "produtos",
        },
      },
    };

    return res.status(200).send(response);
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.updateProduto = async (req, res, next) => {
  try {
    const query = ` UPDATE  produtos SET
                            nome  = ?,
                            preco = ?
                    WHERE
                            id_produto = ?`;
    await mysql.execute(query, [
      req.body.nome,
      req.body.preco,
      req.body.id_produto,
    ]);

    const response = {
      mensagem: "Produto atualizado com sucesso",
      produtoAtualizado: {
        id_produto: req.body.id_produto,
        nome: req.body.nome,
        preco: req.body.preco,
        request: {
          tipo: "GET",
          descricao: "Retorna detalhes de um produto",
          url: process.env.URL_API + "produtos/" + req.body.id_produto,
        },
      },
    };

    return res.status(202).send(response);
  } catch (error) {
    return res.status(500).send({ error: error });
  }
};

exports.deleteProduto = async (request, response, next) => {
  try {
    const query = "DELETE FROM produtos WHERE id_produto = ?";
    await mysql.execute(query, [request.body.id_produto]);

    const resultado = {
      mensagem: "Produto removido com sucesso",
      request: {
        tipo: "POST",
        descricao: "Insere um produto",
        url: process.env.URL_API + "produtos",
        body: {
          nome: "String",
          preco: "Number",
        },
      },
    };

    return response.status(202).send(resultado);
  } catch (error) {
    return response.status(500).send({ error: error });
  }
};
