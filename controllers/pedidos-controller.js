const mysql = require("../mysql").pool;

exports.getPedidos = (request, response, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return response.status(500).send({ error: error });
    }

    connection.query(
      `     SELECT pedidos.id_pedido,
                   pedidos.quantidade,
                   produtos.id_produto,
                   produtos.nome,
                   produtos.preco
              FROM pedidos
        INNER JOIN produtos
                ON produtos.id_produto = pedidos.id_produto`,

      (error, result, field) => {
        connection.release();

        if (error) {
          return response.status(500).send({ error: error });
        }

        const resultado = {
          quantidade: result.length,
          pedidos: result.map((pedido) => {
            return {
              id_pedido: pedido.id_pedido,
              quantidade: pedido.quantidade,
              produto: {
                id_produto: pedido.id_produto,
                nome: pedido.nome,
                preco: pedido.preco,
              },
              request: {
                tipo: "GET",
                descricao: "Retorna detalhes de um pedido",
                url: "http://localhost:3000/pedidos/" + pedido.id_pedido,
              },
            };
          }),
        };

        return response.status(200).send(resultado);
      }
    );
  });
};

exports.postPedido = (request, response, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return response.status(500).send({ error: error });
    }

    connection.query(
      "SELECT * FROM produtos WHERE id_produto = ?",
      [request.body.id_produto],
      (error, result, field) => {
        if (error) {
          return response.status(500).send({ error: error });
        }

        if (result.length == 0) {
          return response.status(404).send({
            mensagem: "Não foi encontrado produto com este ID",
          });
        }

        connection.query(
          "INSERT INTO pedidos (id_produto, quantidade) VALUE (?,?)",
          [request.body.id_produto, request.body.quantidade],
          (error, result, field) => {
            connection.release();

            if (error) {
              return response.status(500).send({ error: error });
            }

            const resultado = {
              mensagem: "Pedido inserido com sucesso",
              pedidoCriado: {
                id_pedido: result.id_pedido,
                id_produto: request.body.id_produto,
                quantidade: request.body.quantidade,
                request: {
                  tipo: "GET",
                  descricao: "Retorna todos os pedidos",
                  url: "http://localhost:3000/pedidos/",
                },
              },
            };

            return response.status(201).send(resultado);
          }
        );
      }
    );
  });
};

exports.getUmPedido = (request, response, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return response.status(500).send({ error: error });
    }

    connection.query(
      "SELECT * FROM pedidos WHERE id_pedido = ?",
      [request.params.id_pedido],
      (error, result, field) => {
        connection.release();

        if (error) {
          return response.status(500).send({ error: error });
        }

        if (result.length == 0) {
          return response.status(404).send({
            mensagem: "Não foi encontrado pedido com este ID",
          });
        }

        const resultado = {
          pedido: {
            id_pedido: result[0].id_pedido,
            id_produto: result[0].id_produto,
            quantidade: result[0].quantidade,
            request: {
              tipo: "GET",
              descricao: "Retorna todos os pedidos",
              url: "http://localhost:3000/pedidos/",
            },
          },
        };

        return response.status(200).send(resultado);
      }
    );
  });
};

exports.deletePedido = (request, response, next) => {
  mysql.getConnection((error, connection) => {
    if (error) {
      return response.status(500).send({ error: error });
    }

    connection.query(
      `DELETE FROM pedidos WHERE id_pedido = ?`,
      [request.body.id_pedido],
      (error, result, field) => {
        connection.release();

        if (error) {
          return response.status(500).send({ error: error });
        }

        const resultado = {
          mensagem: "Pedido removido com sucesso",
          request: {
            tipo: "POST",
            descricao: "Insere um pedido",
            url: "http://localhost:3000/pedidos",
            body: {
              id_produto: "Number",
              quantidade: "Number",
            },
          },
        };
        return response.status(202).send(resultado);
      }
    );
  });
};
